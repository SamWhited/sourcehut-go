// Copyright 2019 The Sourcehut API Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

package sourcehut

// ErrWantArray is exposed in the test package only; it is the one internal
// detail on iterators that needs to be tested but which I don't want to export
// yet.
var ErrWantArray = errWantArray
