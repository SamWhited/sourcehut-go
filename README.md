# Sourcehut Go SDK

[![Issue Tracker](https://img.shields.io/badge/style-todo.sr.ht-green.svg?longCache=true&style=popout-square&label=issues)](https://todo.sr.ht/~samwhited/terraform-provider-sourcehut)
[![Patches](https://img.shields.io/badge/style-lists.sr.ht-blue.svg?longCache=true&style=popout-square&label=patches)][list]
[![GoDoc](https://godoc.org/git.sr.ht/~samwhited/sourcehut-go?status.svg)](https://godoc.org/git.sr.ht/~samwhited/sourcehut-go)


This is the repository for the unofficial [Sourcehut] Go SDK.

[Sourcehut]: https://sourcehut.org/


## Submitting Patches

To submit a patch, first read the [mailing list etiquette] and [contribution]
guides and then send patches to my general purpose patches [mailing list][list].
Please prefix the subject with `[PATCH sourcehut-go]`.
To configure your checkout of this repo to always use the correct prefix and
send to the correct list cd into the repo and run:

    git config sendemail.to ~samwhited/patches@lists.sr.ht
    git config format.subjectPrefix 'PATCH sourcehut-go'

For more information see the file [`CONTRIBUTING.md`].

[mailing list etiquette]: https://man.sr.ht/lists.sr.ht/etiquette.md
[contribution]: https://man.sr.ht/git.sr.ht/send-email.md
[list]: https://lists.sr.ht/~samwhited/patches
[`CONTRIBUTING.md`]: ./CONTRIBUTING.md
