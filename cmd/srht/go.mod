module git.sr.ht/~samwhited/sourcehut-go/cmd/srht

go 1.11

require (
	git.sr.ht/~samwhited/sourcehut-go v0.0.0
	mellium.im/cli v0.1.0
)

replace git.sr.ht/~samwhited/sourcehut-go => ../../
